﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRoll : MonoBehaviour
{
    public float ballspeed;
    public float moveSpeed = 5f;
    public Score force;
    private Vector3 startingPosition;
    private Vector3 startingRotation;
    private Rigidbody ballRigidBody;
    private bool ballIsRolling;

    private void Awake()
    {
        force = GameObject.FindGameObjectWithTag("Force Used").GetComponent<Score>();
    }

    private void Start()
    {
        startingPosition = transform.position;
        startingRotation = transform.rotation.eulerAngles;
        ballRigidBody = GetComponent<Rigidbody>();
        ballIsRolling = false;
        ballspeed = 100f;
    }
    public void ResetBall()
    {
        transform.position = startingPosition;
        ballRigidBody.velocity = Vector3.zero;
        ballRigidBody.angularVelocity = Vector3.zero;
        transform.rotation = Quaternion.Euler(startingRotation);
        GetComponent<Rigidbody>().freezeRotation = true;
        ballIsRolling = false;
    }
    void OnMouseDown()
    {
        if (!ballIsRolling)
        {
            GetComponent<Rigidbody>().freezeRotation = false;
            GetComponent<Rigidbody>().AddForce(ballspeed * transform.forward, ForceMode.Impulse);
            ballIsRolling = true;
        }     
    }
    void Update()
    {
        Vector3 Move = Vector3.zero;
        Move.x = Input.GetAxis("Horizontal");
        transform.position += Move * moveSpeed * Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.W) && !ballIsRolling)
        {
            ballspeed += 50.0f;
            if (ballspeed > 500)
            {
                ballspeed = 50.0f;
            }
        }
        force.Add(ballspeed);
    }
}
