﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverDataDeployer : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI resultText;

    [SerializeField] TextMeshProUGUI pinsAndScore;

    [SerializeField] TextMeshProUGUI shotsLeft;

    void Start()
    {
        if (PinsScoreManager.instance.GetPinsLeft() > 0)
        {
            resultText.text = "You Lost!";  
        }
        else
        {
            resultText.text = "You Won!";
        }
        pinsAndScore.text = "Pins Left " + PinsScoreManager.instance.GetPinsLeft() + "\nScore " + PinsScoreManager.instance.GetScore();
        shotsLeft.text = "Shots Left: " + PinsScoreManager.instance.GetShotsLeft();
    }

    void Update()
    {

    }
}
