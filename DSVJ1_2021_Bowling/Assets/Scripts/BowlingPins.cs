﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingPins : MonoBehaviour
{
    public float threshold = 80f;
    public int point = 1;
    bool pinHasFallen = false;

    void CheckIfFalls()
    {
        if(!pinHasFallen)
        {
            float angleDifference = Vector3.Angle(Vector3.up, transform.up);

            if (angleDifference > threshold)
            {
                PinsScoreManager.instance.IncreaseScoreDecreasePins();
                pinHasFallen = true;
            }
        }      
    }
    private void Update()
    {
        CheckIfFalls();
    }
}
