﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PinsScoreManager : MonoBehaviour
{
    public static PinsScoreManager instance;
    int score;
    int maxPins;
    int shotsLeft;
    public TextMeshProUGUI PinsTextComponent;

    public void IncreaseScoreDecreasePins()
    {
        score++;
        maxPins--;
        PinsTextComponent.text = "Pins Left " + maxPins + "\nScore " + score;
        if(maxPins==0)
        {
            LeaveGame();
        }       
    }

    public void LeaveGame()
    {
        SceneManager.LoadScene("VictoryLossScreen");
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public int GetScore()
    {
        return score;
    }
    public int GetPinsLeft()
    {
        return maxPins;
    }

    public void SetShotsLeft(int shots)
    {
        shotsLeft = shots;
    }
    public int GetShotsLeft()
    {
        return shotsLeft;
    }


    void Start()
    {
        score = 0;
        maxPins = 10;
        PinsTextComponent.text = "Pins Left " + maxPins + "\nScore " + score;
    }

    void Update()
    {

    }
}
