﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotsLeft : MonoBehaviour
{
    private int shotsRemaining = 0;
    public Text shotsLeft;


    public void Reduce()
    {
        if (shotsRemaining > 0)
        {
            shotsRemaining--;
            UpdateDisplay();
            PinsScoreManager.instance.SetShotsLeft(shotsRemaining);
            if (shotsRemaining == 0)
            {
                PinsScoreManager.instance.LeaveGame();
            }
        }     
    }

    private void Start()
    {
        shotsRemaining = 3;
        UpdateDisplay();
    }

    void UpdateDisplay()
    {
        shotsLeft.text = "Shots Left: " + shotsRemaining;
    }
}
