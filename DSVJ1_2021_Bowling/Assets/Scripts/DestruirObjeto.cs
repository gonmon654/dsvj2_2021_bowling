﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestruirObjeto : MonoBehaviour
{
    public ShotsLeft shots;

    private void Awake()
    {
        shots = GameObject.FindGameObjectWithTag("Shots Left").GetComponent<ShotsLeft>();    
    }

    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.GetComponent<BallRoll>())
        {
            obj.gameObject.GetComponent<BallRoll>().ResetBall();
            shots.Reduce();
        }
    }
}
